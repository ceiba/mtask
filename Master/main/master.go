package main

import (
	"MserviceTask/Common"
	"MserviceTask/Master"
	"flag"
	"log"
	"runtime"
	"time"
)

var (
	confFile string // 配置文件路径
)

// 解析命令行参数
func initArgs() {
	// master -config ./master.conf -xxx
	// master -h
	flag.StringVar(&confFile, "config", "./master.conf", "指定master.conf")
	flag.Parse()
}

// 初始化线程数量
func initEnv() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}
func main() {
	var (
		err error
	)
	initArgs()
	initEnv()
	//创建配置文件
	if err = Common.NewConf(confFile); err != nil {
		goto ERR
	}
	//数据库
	if err = Common.NewMysqldb(); err != nil {
		goto ERR
	}
	//创建任务管理器
	if err = Master.NewJobManage(); err != nil {
		goto ERR
	}
	//启动HTTP服务
	if err = Master.NewGHttpService(); err != nil {
		goto ERR
	}
	for {
		time.Sleep(1 * time.Second)
	}
	return
ERR:
	log.Print(err)
}
