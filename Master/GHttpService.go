package Master

import (
	"MserviceTask/Common"
	"MserviceTask/Master/HttpServices"
	"github.com/gin-gonic/gin"
	`net/http`
)

type GHttpService struct {
	*gin.Engine
}

var (
	G_GhttpService *GHttpService
)

func NewGHttpService() (err error) {
	var (
		r *gin.Engine
	)
	r = gin.Default()
	r.Static("/admin", Common.G_CONF.String("HTTP.htmlRoot"))
	r.StaticFS("/more_static", http.Dir(Common.G_CONF.String("HTTP.htmlRoot")))
	r.StaticFile("/favicon.ico", "./resources/favicon.ico")

	r.GET("/api/v1/task/list/", HttpServices.HandelTaskList)
	G_GhttpService = &GHttpService{
		r,
	}
	go r.Run(":" + Common.G_CONF.String("HTTP.port"))
	return
}
