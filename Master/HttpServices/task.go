package HttpServices

import (
	Common2 "MserviceTask/Common"
	"MserviceTask/Master/Common"
	`fmt`
	"github.com/gin-gonic/gin"
	"log"
)

//任务管理列表
type Task struct {
	Base
}
var(
	Page Common.Page
)
//任务列表
func (t *Task) List(q *Common.TaskListReq) (r []*Common.TaskResp, err error) {
	var (
		sql  string
		countSql string
		resp []*Common.TaskResp
	)
	sql = "SELECT * FROM ec_admin limit 20"
	countSql="SELECT count(*) count FROM ec_admin"
	Common2.G_MYSQLDB.Database.Get(&t.Page,countSql)
	if t.Page.Count<=0 {
		err=fmt.Errorf("empty data")
		return
	}
	Page=t.Page
	log.Println(t.Page.Count)
	if err = Common2.G_MYSQLDB.Database.Find(&resp, sql); err != nil {
		log.Println(err)
		return
	}
	r = resp
	return
}
func HandelTaskList(c *gin.Context) {
	var (
		r   []*Common.TaskResp
		err error
	)
	if r, err = (&Task{}).List(nil); err != nil {
		log.Print(err)
		return
	}

	log.Println("===============",Page.Count)

	(&Task{Base{Page:Page}}).Data(c, r)
}
