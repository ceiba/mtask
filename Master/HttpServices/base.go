package HttpServices

import (
	`MserviceTask/Master/Common`
	"github.com/gin-gonic/gin"
)

type Base struct {
	Page   Common.Page
}

//渲染数据
func (t *Base) Data(ctx *gin.Context, data interface{}) {
	var (
		msg map[string]interface{}
	)
	msg = make(map[string]interface{})
	msg["data"] = data
	msg["info"] = "Success"
	msg["code"] = 200
	msg["count"]=t.Page.Count
	ctx.JSON(200, msg)
}

//显示成功
func (t *Base) Success(ctx *gin.Context, message string, code int) {
	var (
		msg map[string]interface{}
	)
	msg = make(map[string]interface{})
	msg["data"] = ""
	msg["info"] = message
	msg["code"] = code
	ctx.JSON(200, msg)
}

//显示失败
func (t *Base) Error(ctx *gin.Context, message string, code int) {
	var (
		msg map[string]interface{}
	)
	msg = make(map[string]interface{})
	msg["data"] = ""
	msg["info"] = message
	msg["code"] = code
	ctx.JSON(500, msg)
}
