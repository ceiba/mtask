package Common

type TaskListReq struct {
	TaskId int64 `json:"taskId"`
	TypeId int   `json:"typeId"`
}
