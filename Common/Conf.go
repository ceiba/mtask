package Common

import (
	"fmt"
	"github.com/aWildProgrammer/fconf"
	"log"
)

type Conf struct {
	*fconf.Config
}

var (
	G_CONF *Conf
)

func NewConf(configPath string) (err error) {
	var (
		config *fconf.Config
	)
	if !fconf.PathExists(configPath) {
		err = fmt.Errorf(configPath + " file is null")
		goto ERR
	}
	if config, err = fconf.NewFileConf(configPath); err != nil {
		goto ERR
	}
	G_CONF = &Conf{
		config,
	}

ERR:
	log.Print(err)

	return
}
