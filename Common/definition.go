package Common

type JobItem struct {
	Name     string `json:"name"`     //任务名称
	Command  string `json:"command"`  //任务执行的命令
	CronExpr string `json:"cronExpr"` //任务表达式
}
