package Common

import (
	"encoding/json"
	"fmt"
	"github.com/zyx4843/gojson"
)

//JSON返回结构
type JSONStruct struct {
	Code   int
	Data   []interface{}
	Msg    string
	Status int
}

//map[string]interface{}

type JSONStructPan struct {
	Code   int
	Data   map[string]interface{}
	Msg    string
	Status int
}

//https://github.com/widuu/gojson
type Js struct {
	data interface{}
}

//解码
func JsonDecode(jsontr string) map[string]interface{} {
	data := gojson.Json(jsontr).Getdata()
	return data
}

//查找
func FindJson(jsontr, jsonkey string) map[string]interface{} {
	data := gojson.Json(jsontr).Get(jsonkey).Getdata()
	return data
}

func JsonEncode(m map[string]interface{}) (string, error) {
	result, err := json.MarshalIndent(m, "", "    ")
	if err != nil {
		fmt.Println("err = ", err)
		return "", err
	}
	return string(result), nil
}
