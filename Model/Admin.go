package Model

type Admin struct {
	Id        int64  `db:"id"`
	Username  string `db:"username"`
	Logintime string `db:"logintime"`
	Status    int    `db:"status"`
}
